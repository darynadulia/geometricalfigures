import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { GraphComponent } from './components/graph/graph.component';
import { FigureFormComponent } from './components/figure-form/figure-form.component';
import { FormsModule } from '@angular/forms';
import { MainComponent } from './components/main/main.component';
import { InformationComponent } from './components/information/information.component';

@NgModule({
  declarations: [
    AppComponent,
    GraphComponent,
    FigureFormComponent,
    MainComponent,
    InformationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
