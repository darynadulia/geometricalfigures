import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Point } from '../point';
import {Figure} from '../figure';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {
  @Input() points?: Point[];
  figure: Figure;

  constructor() { }

  ngOnInit(): void { }

  ngOnChanges(changes: SimpleChanges): void {
    this.figure = new Figure(this.points);
  }
  

}
