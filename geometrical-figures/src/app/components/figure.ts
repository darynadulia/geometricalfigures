import { Point } from "./point";

export class Figure {
    points: Array<Point>;
    sides: Array<number>;
    type: string;
    perimeter?: number;
    area?: number;
    message?: string;
    constructor(points: Array<Point>) {
        this.points = points;
        this.sides = this.points.map((item, index, arr: Array<Point>) => {
            let j: number = index !== this.points.length - 1 ? index + 1 : 0;
            return this.getLenght(item, arr[j]);
        });
        this.perimeter = Math.round(this.sides.reduce((value, current) => value + current, 0));
        this.type = `${this.points.length}-кутник`;
        this.area = 0;
        this.setArea(this.points);
    }
    private setArea(points: Array<Point>): void {
        if (points.length > 2) {
            const tringleArea = this.getTriangleArea(points.slice(0, 3));
            if (tringleArea > 0) {
                this.area += tringleArea;
                this.setArea(points.slice(0, 2).concat(points.slice(3)));
            }
            else { 
                this.message = "Виникла помилка при обчисленні(";
            }

        }
    }
    private getLenght(point0: Point, point1: Point): number{
        return Math.sqrt(Math.pow((point0.x - point1.x), 2) + Math.pow((point0.y - point1.y), 2))
    }

    private getTriangleArea(points: Array<Point>): number {
        return 0.5 * Math.abs((points[1].x - points[0].x) * (points[2].y - points[0].y) - (points[2].x - points[0].x) * (points[1].y - points[0].y));
    }
}

