import { Component, ElementRef, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import {Point} from '../point';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})

export class GraphComponent implements OnInit {
  @Input() points?: Point[];

  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>; 

  private ctx: CanvasRenderingContext2D;
  
  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges){
      this.ctx = this.canvas.nativeElement.getContext('2d');
      this.ctx.moveTo(this.points[0].x, this.ctx.canvas.height - this.points[0].y);
      for(let i = 1; i < this.points.length; i++){
        this.ctx.lineTo(this.points[i].x, this.ctx.canvas.height - this.points[i].y);
      }
      this.ctx.fillStyle = "#EE6055";
      this.ctx.fill();
  }
}
