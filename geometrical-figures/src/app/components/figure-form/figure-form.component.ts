import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Point } from '../point';

@Component({
  selector: 'app-figure-form',
  templateUrl: './figure-form.component.html',
  styleUrls: ['./figure-form.component.css']
})
export class FigureFormComponent implements OnInit {
  @Output() newItemEvent = new EventEmitter<Point>();
  constructor() { }

  ngOnInit(): void {
  }

  addNewPoint(form: NgForm): void {
    this.newItemEvent.emit(form.value);
  }
}
