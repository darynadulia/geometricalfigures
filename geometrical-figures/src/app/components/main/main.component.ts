import { Component, OnInit } from '@angular/core';
import {Point} from '../point';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  points: Point[] = [];

  addPoint(newPoint: Point): void {
    this.points.push(newPoint); 
    this.points = this.points.slice();
  }

  onClearClick(): void {
    this.points = [];
  }

  constructor() { }

  ngOnInit(): void {
  }

}
